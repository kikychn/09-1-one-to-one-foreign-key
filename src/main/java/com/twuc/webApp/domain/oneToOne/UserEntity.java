package com.twuc.webApp.domain.oneToOne;

import javax.persistence.*;

// TODO
//
// 请创建 UserEntity 和 UserProfileEntity 之间的 one-to-one 关系。并且确保 UserEntity 的数据
// 表结构如下：
//
// user_entity
// +─────────+─────────+──────────────────────────────+
// | column  | type    | additional                   |
// +─────────+─────────+──────────────────────────────+
// | id      | bigint  | primary key, auto_increment  |
// +─────────+─────────+──────────────────────────────+
//
// <--start-
@Entity
public class UserEntity {
    @Id
    @GeneratedValue
    private Long id;

    @OneToOne(mappedBy = "user", cascade = CascadeType.ALL)
    private UserProfileEntity userProfileEntity = null;

    public UserEntity() {
    }

    public UserProfileEntity getUserProfileEntity() {
        return userProfileEntity;
    }

    public void setUserProfileEntity(UserProfileEntity userProfileEntity) {
        this.userProfileEntity = userProfileEntity;
    }

    public Long getId() {
        return id;
    }
}
// --end-->