package com.twuc.webApp.domain.oneToOne;

import com.twuc.webApp.domain.ClosureValue;
import com.twuc.webApp.domain.JpaTestBase;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.NoSuchElementException;

import static org.junit.jupiter.api.Assertions.*;

class UserAndProfileTest extends JpaTestBase {
    @Autowired
    private UserEntityRepository userEntityRepository;

    @Autowired
    private UserProfileEntityRepository userProfileEntityRepository;

    @Test
    void should_save_user_and_profile() {
        // TODO
        //
        // 请书写一个测试实现如下的功能：
        //
        // Given 一个 UserEntity 和一个 UserProfileEntity。它们并没有持久化。
        // When 持久化 UserEntity
        // Then UserProfileEntity 也一同被持久化了。
        //
        // <--start--
        ClosureValue<Long> userId = new ClosureValue<>();
        ClosureValue<Long> userProfileId = new ClosureValue<>();

        flushAndClear(em-> {
            UserEntity user = new UserEntity();
            UserProfileEntity userProfile = new UserProfileEntity("zheng", "piaopiao");
            user.setUserProfileEntity(userProfile);
            userEntityRepository.save(user);
            userProfileId.setValue(userProfile.getId());
        });

        run(em->{
            UserProfileEntity userProfile = userProfileEntityRepository
                    .findById(userProfileId.getValue())
                    .orElseThrow(NoSuchElementException::new);
            assertNotNull(userProfile);
        });
        // --end->
    }

    @Test
    void should_remove_parent_and_child() {
        // TODO
        //
        // 请书写一个测试：
        //
        // Given 一个持久化了的 UserEntity 和 UserProfileEntity
        // When 删除 UserEntity
        // Then UserProfileEntity 也被删除了
        //
        // <--start-
        ClosureValue<Long> userId = new ClosureValue<>();
        ClosureValue<Long> userProfileId = new ClosureValue<>();

        flushAndClear(em-> {
            UserEntity user = new UserEntity();
            UserProfileEntity userProfile = new UserProfileEntity("zheng", "piaopiao");
            user.setUserProfileEntity(userProfile);
            userEntityRepository.save(user);
            userId.setValue(user.getId());
            userProfileId.setValue(userProfile.getId());
        });

        flushAndClear(em -> {
            userEntityRepository.deleteById(userId.getValue());
        });

        run(em->{
            assertThrows(NoSuchElementException.class, () -> {
                userProfileEntityRepository
                        .findById(userProfileId.getValue())
                        .orElseThrow(NoSuchElementException::new);
            });
        });
        // --end->
    }
}